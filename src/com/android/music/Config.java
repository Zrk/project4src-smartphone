package com.android.music;

import android.util.Log;

public class Config {
	static private String musiqueDir = "MusicDirectory";
	static private String ip = "192.168.1.12";
	static private String username = "";
	static private String password = "";
	static private Boolean configured = false;
	
	static boolean IsConfigured(){
		if(configured)
			Log.d("4SCR","IsConfigured");
		else
			Log.d("4SCR","not IsConfigured");
		return configured;
	}

	static void setConfigured(boolean c){
		configured = c;
	}
	
	static void setCredentials(String v,String p){
		username = v;
		password = p;
	}
	
	static String getUsername(){
		return username;
	}
	
	static String getPassword(){
		return password;
	}
	
	static String getIpServer(){
		return ip;
	}
	
	static String getMusiqueDir(){
		return musiqueDir;
	} 
}
