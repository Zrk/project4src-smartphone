package com.android.music;

import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

import android.app.ListActivity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class SyncBrowserActivity extends ListActivity 

{	
	
	public FTPFile[] fileArray = null; 
	public FTPClient ftp = null;
	private String[] values;
	private static String destpath =  Environment.getExternalStorageDirectory().getAbsolutePath();
	MyFTPClient ftpDownloader = null;

	
	@Override
	
	protected void onCreate(Bundle savedInstanceState) {
		
	
		
		super.onCreate(savedInstanceState);
		Log.v("4SRC","ouverture de sync");
		setContentView(R.layout.activity_sync_browser);
		setTitle("Synchronisation");

		
		//_______________THREAD____________________________
		
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
        .permitAll()
        .build();   
		StrictMode.setThreadPolicy(policy);

		//______________THREAD____________________________
		
		try{
			
		ftpDownloader = new MyFTPClient();
		ftpDownloader.ftpConnect(Config.getIpServer(), "anonymous", "",21);
		FTPFile[] fileArray = ftpDownloader.getDirectories();
		
		Log.d("4SRC", "ICI c'est paris");
		
	    int length = fileArray.length;
	    Log.d("4SRC", "ICI c'est la taille");
	    		
	    /*String[] values = new String[] { "Blbla", "Jacques Higelin", "Simple List View With ListActivity",
	    "ListActivity Android", "Android ", "Paris Hilton", "ListView ListActivity Array Adapter", "Android Example ListActivity" };*/
	    		
        values = new String[length];
        for (int i = 0; i < length; i++) {  // noms des fichiers dans une chaine de caractere
            values[i] = fileArray[i].getName();
            Log.v("4SRC",values[i]);}
		}
            catch(Exception e){Log.d("4SRC", e.getMessage());
    		e.printStackTrace();}
        
        /* Define a new Adapter
        First parameter - Context
        Second parameter - Layout for the row
        Third - the Array of data */
        Log.d("4SRC", "ICI c'est paris");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
          		   this,
                     android.R.layout.simple_list_item_1, 
                     values);

         setListAdapter(adapter); // Bind to our new adapter.
      
      // Ajout pour la géolocalisation
         
         //Register the BroadcastReceiver
         IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
         filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
       
         registerReceiver(RechercheEmetteur.ActionFoundReceiver, filter); // Don't forget to unregister during onDestroy
     }


	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {

        super.onListItemClick(l, v, position, id);
        Log.v("4SRC","Salutations, faites la synchro");
        int p = position;  
        String e = String.valueOf(p);
        Log.v("4SRC",e); 
        final String path = values[p];
        Log.v("4SRC",path);
        Log.v("4SRC",destpath);
  
        try {
			new Thread()
			{
			      @Override
			      public void run() {
			          try {
			              ftpDownloader.ftpDownloadDirectory(path);
			              Log.d("4SRC","FTP File downloaded successfully");

			          } catch (Exception e) {
			              Log.d("4SRC","dkqlfhglsdkqghkFTP File downloaded successfully");
			          }
			      }
			  }.start();
        } catch (Exception exp) {
            exp.printStackTrace();
        }	
  }
        

	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.sync_browser, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	 }*/
	@Override
	public void onDestroy() {
		
	    unregisterReceiver(RechercheEmetteur.ActionFoundReceiver);
	    super.onDestroy();
	}
}

    


	 