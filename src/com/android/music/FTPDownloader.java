package com.android.music;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;

import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
 
 
public class FTPDownloader {
 
    FTPClient ftp = null;
	private static String destpath =  Environment.getExternalStorageDirectory().getAbsolutePath();

 
    public FTPDownloader(String host, String user, String pwd) throws Exception {
        ftp = new FTPClient();
        ftp.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));
        int reply;
        ftp.connect(host);
        reply = ftp.getReplyCode();
        if (!FTPReply.isPositiveCompletion(reply)) {
            ftp.disconnect();
            throw new Exception("Exception in connecting to FTP Server");
        }
        ftp.login(user, pwd);
        ftp.setFileType(FTP.BINARY_FILE_TYPE);
        ftp.enterLocalPassiveMode();
    }
    public void downloadFiles(String u,String d)
    {
        try {
        	Log.d("4SRC","df554sddsg for"+destpath+"toto.txt");
        	FileOutputStream fos = new FileOutputStream(destpath+"/toto.txt");
            this.ftp.retrieveFile("toto.txt", fos);
        } catch (IOException fe) {
        	Log.d("4SRC","dfsddsg for");
            fe.printStackTrace();
        }
    }
    public class DownloadFilesTask extends AsyncTask<String, Integer, Long> {
    	private FTPClient m_ftp = null;
    	protected void setFtp(FTPClient _ftp )
    	{
    		m_ftp = _ftp;
    	}
    	
        protected Long doInBackground(String... urls) {
            int count = urls.length;
            long totalSize = 0;
            for (int i = 0; i < count; i++) {
            	Log.d("4SRC","bouckle for");
            	this.downloadFile(urls[i],destpath+"/toto.txt");
                // Escape early if cancel() is called
                if (isCancelled()) break;
            }
            return totalSize;
        }
        public void downloadFile(String remoteFilePath, String localFilePath) {
            try {
            	Log.d("4SRC","dfsddsg for"+localFilePath);
            	FileOutputStream fos = new FileOutputStream(localFilePath);
               // this.m_ftp.retrieveFile(remoteFilePath, fos);
            } catch (IOException e) {
            	Log.d("4SRC","dfsddsg for");
                e.printStackTrace();
            }
        }
        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(Long result) {
        }
    }
 
    public FTPClient getFTPClient(){
    	return ftp;
    }
    
    public void disconnect() {
        if (this.ftp.isConnected()) {
            try {
                this.ftp.logout();
                this.ftp.disconnect();
            } catch (IOException f) {
                // do nothing as file is already downloaded from FTP server
            }
        }
    }

}