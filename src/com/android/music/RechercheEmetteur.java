package com.android.music;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.UUID;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;

// Thibault

import com.android.music.EnvoiRequete;
import com.android.music.Data;


public class RechercheEmetteur{
	
	  static public BluetoothAdapter btAdapter; 
	  static private ArrayList<BluetoothDevice> btDeviceList = new ArrayList<BluetoothDevice>();
	  static private ArrayList<String> btDeviceListNames = new ArrayList<String>();
	  static short Rssimax = -300;
	  static short Rssipiece1 = -300;
	  static short Rssipiece2 = -300;
	  static short Rssip1 = -300;
	  static String DeviceRssiMax="None yet";
	static private BluetoothDevice device2; 
	static int ok_connect = 1; // ok_connect = 1  : pas de device auquel se connecter ; ok_connect = 0 : il existe des devices auxquels on peut se connecter
	
	static public ConnectThread bt = null;
	  private static final int REQUEST_ENABLE_BT = 1;
	
	  static private ConnectedThread connectedthread = null;
	  static private EnvoiRequete http_thread = null;
	  static private long lastTime = 0;
	  
	  static Context context;
	  
	
	public RechercheEmetteur(Context c){
		context = c;
	 // Getting the Bluetooth adapter
	    btAdapter = BluetoothAdapter.getDefaultAdapter();
	    
	    
	    new Thread() {
			@Override public void run() {
	
	   
	    String msg2 = "4SRC Enceintes intelligentes : ";
	    final int i = 0;
	    String istring;
	    istring = Integer.toString(i);
	    
	    
	    // Mise en marche de Bluetooth
	    if(btAdapter == null){}
	    else{
	    if(!btAdapter.isEnabled()){
	    	btAdapter.enable();
	    }
	    }
	    
	    while(!btAdapter.isEnabled()){
	    	
	    }
	    
	    btAdapter.startDiscovery(); 
	   
	    Log.d(msg2, "Started Discovery : interation num " + istring);
	   
			}
			}.start();
	   
	    
	} 

    
   
// ActionFoundReceiver : recherche les �metteurs Bluetooth, et se connecte au plus proche
		
	public  static final BroadcastReceiver ActionFoundReceiver = new BroadcastReceiver(){
		   // @Override

		    public void onReceive(Context context, Intent intent) {
		     String action = intent.getAction();
		     // Cette partie s'effectue lorsque qu'un �metteur Bluetooth a �t� trouv� 
		     if(BluetoothDevice.ACTION_FOUND.equals(action)) {
		    
		       ok_connect = 0; 
		       BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE); // r�cup�ration du nom du p�riph�rique Bluetooth
		       short Rssi = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI,Short.MIN_VALUE);  // R�cup�ration du RSSI du p�riph�rique
		       
		       btDeviceList.add(device);
		       btDeviceListNames.add(device.getName());
		       
		     if (device.getName().contains("p1")){
		    	   String msg2 = "4SRC Enceintes intelligentes : ";
		    	   Log.d(msg2, "Found " + device.getName() + ", RSSI : " + Rssi);
		    	   Rssip1=  Rssi;
		    	  if (Rssi > Rssimax){
		      
		    	  device2 = device;
		    	  DeviceRssiMax = device.getName();
		    	  Rssimax = Rssi;
		    	 
		    	  }
		    
		     }
		     
		     if (device.getName().contains("piece1")){
		    	   String msg2 = "4SRC Enceintes intelligentes : ";
		    	   Log.d(msg2, "Found " + device.getName() + ", RSSI : " + Rssi);
		    	   Rssipiece1 = Rssi;
		    	  if (Rssi > Rssimax){
		      
		    	  device2 = device;
		    	  DeviceRssiMax = device.getName();
		    	  Rssimax = Rssi;
		    	  }
		    
		     }
		     
		     if (device.getName().contains("piece2")){
		    	   String msg2 = "4SRC Enceintes intelligentes : ";
		    	   Log.d(msg2, "Found " + device.getName() + ", RSSI : " + Rssi);
		    	   Rssipiece2 = Rssi;
		    	  if (Rssi > Rssimax){
		      
		    	  device2 = device;
		    	  DeviceRssiMax = device.getName();
		    	  Rssimax = Rssi;
		    	  }
		    	  
		    	
		     }


		      // if (btDeviceListNames.contains("p1")){
		    	  if (btDeviceListNames.contains("piece1")){     // On acc�l�re la fin de la phase de d�couverte
		    		  
		    		  if (btDeviceListNames.contains("piece2")){
		    			
		    	   btAdapter.cancelDiscovery();
		    	   String msg2 = "4SRC Enceintes intelligentes : ";
		    	   Log.d(msg2, "Found piece1 and piece2 before discovery was finished");
		    		 // }
		    		  }
		    	   }
		    	  
		     }
		            if(BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) { // Une fois que la phase de d�couverte est finie, choix du RSSI le plus �lev�
		            	
		          
		         
		            	if(ok_connect == 0){
		            		
		            		   if (btDeviceList.isEmpty()){
		  		            	 btAdapter.startDiscovery();
		  		             }
		            
	
		            String msg2 = "4SRC Enceintes intelligentes : ";
		             Log.d(msg2, "Discovery finished");
		               
				     Log.d(msg2, "Highest RSSI is: " + Rssimax + ", device: "+ DeviceRssiMax);
				     
				     if ( (-10 > (Rssipiece1 - Rssipiece2)) || (10 < (Rssipiece1 - Rssipiece2)) ) {
		            bt = new ConnectThread(device2);
		            bt.start();
		            Log.d(msg2, "Connected to "+DeviceRssiMax);
				     }
				     
				     else {
				    	 Log.d("4SRC Enceintes intelligentes", "Ecart de puissance non suffisant pour choisir le bon �metteur, on ne se connecte pas");
				    	 btAdapter.startDiscovery();
				     }
				     
		             Rssimax=-300;
		             Rssipiece1 = -300;
		             Rssipiece2 = -300;
		             device2 = null;
		             ok_connect = 1;
		         	btDeviceListNames.clear();
	            	btDeviceList.clear();	
		         
		          
		            }
		            	if (ok_connect == 1) {};
		            	
		            }
		            }
		    
		  }; 
		  
		  
		  // Classe pour se connecter � l'�metteur
		  
		   private static class ConnectThread extends Thread {
			    private  BluetoothSocket mmSocket;
			    private  BluetoothDevice mmDevice;
			   
			    public ConnectThread(BluetoothDevice device) {
			        // Use a temporary object that is later assigned to mmSocket,
			        // because mmSocket is final
			        BluetoothSocket tmp = null;
			        mmDevice = device;
			        
			 
			        // Get a BluetoothSocket to connect with the given BluetoothDevice
			        try {
			        				        	 
			        	 Method m;
			        	   try {
			        	    	m = device.getClass().getMethod("createRfcommSocket", new Class[] {int.class         });
			        	        tmp = (BluetoothSocket) m.invoke(device, 1);  
			        	        } catch (SecurityException e) {
			        	    	Log.e("4SRC Enceintes intelligentes", "create() failed", e);
			        	    	} catch (NoSuchMethodException e) {
			        	    	Log.e("4SRC Enceintes intelligentes", "create() failed", e);
			        	    	} catch (IllegalArgumentException e) {
			        	    	Log.e("4SRC Enceintes intelligentes", "create() failed", e);
			        	    	} catch (IllegalAccessException e) {
			        	    	Log.e("4SRC Enceintes intelligentes", "create() failed", e);
			        	    	} catch (InvocationTargetException e) {
			        	    	Log.e("4SRC Enceintes intelligentes", "create() failed", e);
			        	    	}         

			        	
			        	 
			        	 
			        	 Log.d("4SRC Enceintes intelligentes : ", "Created the RFCOMM");
			        } //catch (IOException e) {     Log.d("4SRC Enceintes intelligentes : ", "Couldn't create the RFCOMM"); }
			      finally { mmSocket = tmp;}
			   
			    }
			 
			    public void run() {
			        // Cancel discovery because it will slow down the connection
			        btAdapter.cancelDiscovery();
			        if(btAdapter.isDiscovering()){
			        	btAdapter.cancelDiscovery();
			        }
			 
			        try {
			            // Connect the device through the socket. This will block
			            // until it succeeds or throws an exception
			        	if (mmSocket.isConnected()){
			        		Log.d("4SRC Enceintes intelligentes : ", "Not going to work because socket is already connected!");
			        	}
			            mmSocket.connect();
			          
			        } catch (IOException connectException) {
			        	connectException.printStackTrace();
			        	Log.d("4SRC Enceintes intelligentes : ", "Couldn't connect");
			        	
			        	
			            // Unable to connect; close the socket and start discovery again
			            try {
			                mmSocket.close();
			                Log.d("4SRC Enceintes intelligentes : ", "Closed the socket");
			                btAdapter.startDiscovery();
			                
			                
			            } catch (IOException closeException) {Log.d("4SRC Enceintes intelligentes : ", "Couldn't close the socket"); }
			            return;
			        }
			       
			 
			        // Do work to manage the connection (in a separate thread)
			
			        connectedthread = new ConnectedThread(mmSocket);
			        connectedthread.start();
			       
			       
			    }
			 
			    /** Will cancel an in-progress connection, and close the socket */
			    public void cancel() {
			        try {
			            mmSocket.close();
			            Log.d("4SRC Enceintes intelligentes : ", "Cancelling connection");
			        } catch (IOException e) { }
			    }
			}
		  
		  // Classe pour r�cup�rer les flux de donn�es transmises
		  
		  static private class ConnectedThread extends Thread {
	
			    
			    private  BluetoothSocket mmSocket;
			    private  InputStream mmInStream;
			    private OutputStream mmOutStream;
			  
			  
			    public ConnectedThread(BluetoothSocket socket) {
			        mmSocket = socket;
			        InputStream tmpIn = null;
			        OutputStream tmpOut = null;
			       
			        // Get the input and output streams, using temp objects because
			        // member streams are final
			        try {
			            tmpIn = socket.getInputStream();
			            tmpOut = socket.getOutputStream();
			            Log.d("4SRC Enceintes intelligentes : ", "Created socket!");
			        } catch (IOException e) { Log.d("4SRC Enceintes intelligentes : ", "Couldn't create socket!"); }
			 
			        mmInStream = tmpIn;
			        mmOutStream = tmpOut;
			    }
			 
			    public void run() {
			     String data ;
			     //thibault
			     Data geodata = new Data();
					
					
					
					int l = 0;
					
					while (l<2){
						try {
						
							if(mmInStream.available() > 0) {

								
							
								byte buffer[] = new byte[1];
								
							    int k = mmInStream.read(buffer, 0, 1);
								
                                l = l+1;
								if(k > 0) {
									byte rawdata[] = new byte[k];
									for(int i=0;i<k;i++){
										rawdata[i] = buffer[i];
										byte rawwdata;
										rawwdata = buffer[i];
										if (rawwdata == '\n'){
										}
										else {
											
									if (buffer[i] == 'a'){
										data = "a";
										//Thibault
										geodata.setGeoloc(data);
										http_thread = new EnvoiRequete("http://"+Config.getIpServer()+"/smartphone",geodata);
								        http_thread.start();
								       
										
										//thibault
										 String msg2 = "4SRC Enceintes intelligentes : ";
										
										Log.d(msg2, "Room number : a");
										
									}
									else { 
										if (buffer[i] == 'b') {
											data = "b";
			
											//Thibault
											geodata.setGeoloc(data);
											http_thread = new EnvoiRequete("http://"+Config.getIpServer()+"/smartphone",geodata);
									        http_thread.start();
									       
											 String msg2 = "4SRC Enceintes intelligentes : ";
											
											Log.d(msg2, "Room number : b");

										}
										else { 
											if (buffer[i] == 'c'){
												data = "c";
												geodata.setGeoloc(data);
												http_thread = new EnvoiRequete("http://"+Config.getIpServer()+"/smartphone",geodata);
										        http_thread.start();
												 String msg2 = "4SRC Enceintes intelligentes : ";
												
												Log.d(msg2, "Room number : c");

											}
											else {
												if (buffer[i] == 'd'){ data = "d";
												geodata.setGeoloc(data);
												http_thread = new EnvoiRequete("http://"+Config.getIpServer()+"/smartphone",geodata);
										        http_thread.start();
												 String msg2 = "4SRC Enceintes intelligentes : ";
												
												Log.d(msg2, "Room number : d");
												
												}
											}
										}
									}
									
								
                                    
									
									}
									}
								if (l == 2){
									//bt.cancel();
									int m = 0;
									while (m <200){m+=1;}
									cancel();
							
								    btAdapter.startDiscovery();
								    Log.d("4SRC Enceintes intelligentes : ", "Discovery started");
									}
									
						    	   
							
									}
									}
							
								}
							
						 catch (IOException e) {
							e.printStackTrace();
							cancel();
						}
					
				}
			
			       
			    }
			 
			
			 
			    /* Call this from the main activity to shutdown the connection */
			    public void cancel() {
			     
                        btAdapter.cancelDiscovery();
                        Log.d("4SRC Enceintes intelligentes : ", "Closing streams and sockets. Ready to start again !");
			            if (mmOutStream != null) { try  { mmOutStream.close(); }   catch (Exception e)  { Log.e("Exception", "while closing outstream"+e.toString());  } mmOutStream = null; }
			            if (mmInStream  != null) { try  { mmInStream.close();  }   catch (Exception e)  { Log.e("Exception", "while closing inputstream"+e.toString());} mmInStream  = null; }
			            if (mmSocket    != null) { try  { mmSocket.close();    }   catch (Exception e)  { Log.e("Exception", "while closing socket"+e.toString());     } mmSocket    = null; }
			            ok_connect = 1;
			            //bt = null;
			    }// catch (IOException e) { }
			    }
			
		  static public void closesocket(){
		  
			  Log.d("4SRC Enceintes intelligentes : ", "going to try and cancel, this part is for the on destroy");
			  if (bt == null){
				  Log.d("4SRC Enceintes intelligentes : ", "bt was null!");
			  }
				
			  try{
				connectedthread.cancel();
				//connectedthread =  null;
				bt.cancel();
				//bt = null;
				
				Rssimax = -300;
				DeviceRssiMax="None yet";
			    device2 = null;
				btDeviceListNames.clear();
            	btDeviceList.clear();
			  }
			 catch(NullPointerException e){e.printStackTrace();}
			}

}
