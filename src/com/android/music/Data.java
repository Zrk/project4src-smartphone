package com.android.music;

public class Data {
	private String playlist;
    private String volume;
    private String geoloc;
    private String frame;
    
    
    public String getPlaylist(){
    	return this.playlist;
    }
    public String getVolume(){
    	return this.volume;
    }
    public String getGeoloc(){
    	return this.geoloc;	
    }

	public String getFrame() {
		return this.frame;
	}
    public void setPlaylist(String playlist1){
    	this.playlist=playlist1;
    }
    public void setVolume(String volume1){
    	this.volume=volume1;
    }
    public void setGeoloc(String geoloc1){
    	this.geoloc=geoloc1;
    }
    public void setFrame(long frame1){
    	this.frame=String.valueOf(frame1);
    }
	
}
