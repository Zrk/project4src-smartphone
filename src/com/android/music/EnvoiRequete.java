package com.android.music;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

public class EnvoiRequete extends Thread {
	
	private String url;
	private Data data;
	public EnvoiRequete(String urls, Data datas){
		url=urls;
		data=datas;	
	}
	
	InputStream inputStream = null;
    String result = "";
    Context context;
    public void notif(Context c) {
    	context = c;
    }
   
    public void run(){
    try {

    	Log.d("4SRC Enceintes intelligentes", "d�but JSON");
        // 1. create HttpClient
        HttpClient httpclient = new DefaultHttpClient();

        // 2. make POST request to the given URL
        HttpPost httpPost = new HttpPost(url);

        String json = "";

        // 3. build jsonObject
        JSONObject jsonObject = new JSONObject();
        jsonObject.accumulate("playlist", data.getPlaylist());
        jsonObject.accumulate("username", Config.getUsername());
        jsonObject.accumulate("password", Config.getPassword());
        jsonObject.accumulate("volume", data.getVolume());
        jsonObject.accumulate("geoloc", data.getGeoloc());
        jsonObject.accumulate("frames", data.getFrame());
        jsonObject.accumulate("frames_type_ms", "true");

        // 4. convert JSONObject to JSON to String
        json = jsonObject.toString();

        // ** Alternative way to convert Person object to JSON string usin Jackson Lib 
        // ObjectMapper mapper = new ObjectMapper();
        // json = mapper.writeValueAsString(person); 

        // 5. set json to StringEntity
        StringEntity se = new StringEntity(json);

        // 6. set httpPost Entity
        httpPost.setEntity(se);

        // 7. Set some headers to inform server about the type of the content   
        httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/json");

        Log.d("4SRC Enceintes intelligentes", "intermediaire JSON");
        
        // 8. Execute POST request to the given URL
        HttpResponse httpResponse = httpclient.execute(httpPost);

        Log.d("4SRC Enceintes intelligentes", "fin JSON");
        // 9. receive response as inputStream
        inputStream = httpResponse.getEntity().getContent();
        
     


        // 10. convert inputstream to string
        if(inputStream != null){
            result = convertInputStreamToString(inputStream);
        		Log.d("4SRC",result);
        		
		        Handler h = new Handler(context.getMainLooper());

		        h.post(new Runnable() {
		            @Override
		            public void run() {
		            	try{
		                JSONObject jsonObject = new JSONObject(result);
		                String msg = jsonObject.getString("error");
		                Log.d("4SRC","From server: " + msg);
		                if(msg != "")
		                	Toast.makeText(context,msg,Toast.LENGTH_LONG).show();
		                if(!msg.contains("not logged")){
		                	Log.d("4SRC","setConfigured(true)");
		                	Config.setConfigured(true);
		                }
		            	} catch(Exception e) {

		                    Log.v("4SRC", e.getMessage());
		            	}
		            }
		        });	

        		}
        else
            result = "Did not work!";
        
       

    } catch (Exception e) {
        Log.e("4SRC", "exception",e);
    }


}
    

    private static String convertInputStreamToString(InputStream inputStream) throws IOException{
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

}
